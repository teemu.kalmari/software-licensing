// 3. Cleanup(in listener way)

const exitHandler = (options, exit_code) => {
    console.log(`Servise stopped with exit code: ${exit_code}`);
};

['exit', 'SIGINT', 'SIGUSR1', 'SIGUSR2', 'uncaughtExeption','SIGTERM'].forEach((ev) => {
    process.on(ev, exitHandler.bind(null, ev));
});

// 1. initialize

console.log("Sevice starting.");
require('dotenv').config();
const router = require('./router');
const getPort = () => {
    let port = undefined;
    try {
        port = Number.parseInt(process.env.PORT, 10);
    } catch (err) {
        console.error(`Enviroment variable PORT isn't defined`);
        process.exit(0);

    }
    return port;
}
const PORT = getPort();
const HOST = '0.0.0.0';

// 2. Operate

router.listen(
    PORT,
    HOST,
    () => console.log(`Listening to http://${HOST}:${PORT}`)
)
